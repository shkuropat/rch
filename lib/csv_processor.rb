require 'csv'

class CsvProcessor

  DEFAULT_CSV_OPTIONS = { :col_sep => "\t", :headers => :first_row }
  LINES_PER_FILE = 120000

  class << self
    def save_index_files(merger, output)
      done = false
      file_index = 0
      file_name = output.gsub('.txt', '')
      while not done do
        CSV.open(file_name + "_#{file_index}.txt", "wb", { :col_sep => "\t", :headers => :first_row, :row_sep => "\r\n" }) do |csv|
          headers_written = false
          line_count = 0
          while line_count < LINES_PER_FILE
            begin
              merged = merger.next
              if not headers_written
                csv << merged.keys
                headers_written = true
                line_count +=1
              end
              csv << merged
              line_count +=1
            rescue StopIteration
              done = true
              break
            end
          end
          file_index += 1
        end
      end
    end

    def parse(file)
      CSV.read(file, DEFAULT_CSV_OPTIONS)
    end

    def lazy_read(file)
      Enumerator.new do |yielder|
        CSV.foreach(file, DEFAULT_CSV_OPTIONS) do |row|
          yielder.yield(row)
        end
      end
    end

    def write(content, headers, output)
      CSV.open(output, "wb", { :col_sep => "\t", :headers => :first_row, :row_sep => "\r\n" }) do |csv|
        csv << headers
        content.each do |row|
          csv << row
        end
      end
    end
  end

end
