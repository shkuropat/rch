require File.expand_path('csv_processor',File.dirname(__FILE__))
require File.expand_path('merger',File.dirname(__FILE__))

class Modifier
  OUTPUT_FILE_POSTFIX = '.sorted'
  HEADER_INDEX = 'Clicks'

  def initialize(saleamount_factor, cancellation_factor)
    @saleamount_factor = saleamount_factor
    @cancellation_factor = cancellation_factor
  end

  def modify(output, input)
    input = sort(input)

    input_enumerator = CsvProcessor.lazy_read(input)
    merger = Merger.new(@saleamount_factor, @cancellation_factor)

    output_enumerator = merger.process(input_enumerator)
    CsvProcessor.save_index_files(output_enumerator, output)
  end

  private
  def sort(file)
    output = "#{file}#{OUTPUT_FILE_POSTFIX}"
    content_as_table = CsvProcessor.parse(file)
    headers = content_as_table.headers
    index_of_key = headers.index(HEADER_INDEX)
    content = content_as_table.sort_by { |a| -a[index_of_key].to_i }
    CsvProcessor.write(content, headers, output)
    return output
  end
end

