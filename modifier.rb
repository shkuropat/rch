#/usr/bin/env ruby
require 'pry'
require File.expand_path('lib/modifier',File.dirname(__FILE__))
require 'date'
require 'optparse'

DEFAULT_MODIFICATION_FACTOR = 1
DEFAULT_CANCELLATION_FACTOR = 0.4

def latest(name)
  files = Dir["#{ ENV["PROJECTS_WORKSPACE"] }/*#{name}*.txt"]

  # TODO optimize me
  files = files.select do |file| 
    last_date = /\d+-\d+-\d+_[[:alpha:]]+\.txt$/.match file
    last_date = last_date.to_s.match /\d+-\d+-\d+/
    last_date
  end
  files.sort_by! do |file|
    last_date = /\d+-\d+-\d+_[[:alpha:]]+\.txt$/.match file
    last_date = last_date.to_s.match /\d+-\d+-\d+/
    date = DateTime.parse(last_date.to_s)
    date
  end

  throw RuntimeError if files.empty?

  files.last
end



options = {
  modification_factor: DEFAULT_MODIFICATION_FACTOR,
  cancellaction_factor: DEFAULT_CANCELLATION_FACTOR
}
OptionParser.new do |opts|
  opts.banner = "Usage: modifier.rb :name [options]"

  opts.on("-m", "--modification_factor", "Modification factor") do |m|
    options[:modification_factor] = m
  end
  opts.on("-c", "--cancellation_factor", "Cancellation factor") do |c|
    options[:modification_factor] = c
  end
end.parse!

unless ARGV[0]
  $stderr.puts "Name not set"
  exit 1
end
modified = input = latest(ARGV[0])
modification_factor = 1
cancellaction_factor = 0.4
modifier = Modifier.new(modification_factor, cancellaction_factor)
modifier.modify(modified, input)

puts "DONE modifying"
